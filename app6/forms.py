from django.forms import ModelForm
from .models import Book

class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = ["data"]

# class FriendForm(forms.Form):
#     form_name = forms.CharField(label='Your name', max_length=30, required=True)
#     form_hobby = forms.CharField(label='Your hobby', max_length=30, required=False)
#     form_fav = forms.CharField(label='Your favourite food/drink', max_length=30, required=False)
#     form_year = forms.ChoiceField(choices={x for x in ClassYear.objects.all()}, required=True)
    
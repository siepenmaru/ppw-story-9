from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Book(models.Model):
    data = models.TextField()
    
    def __str__(self):
        return "" + self.title + " with " + self.likes + " Likes"
    
class OpinionBoard(models.Model):
    data = models.TextField()
    
    def __str__(self):
        return "" + self.title + " with " + self.likes + " Likes"
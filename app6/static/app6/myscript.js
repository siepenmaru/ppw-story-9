$(document).ready(function(){	

   $("#myform").submit(function(){

   	  var search = $("#books").val();
   	  if(search == "")
   	  {
   	  	alert("Please enter something in the field");
   	  }
   	  else{		
         var url = "";
         var img = "";
         var title = "";
         var author = "";

         var modal_btn = $('<button onclick="display()" id="display-button" style="text-align: center;">Display top 5 books</button>');
         modal_btn.appendTo('#display-btn');

   	   $.get("https://www.googleapis.com/books/v1/volumes?q=" + search,function(response){

            for(i=0;i<response.items.length;i++)
            {
               addRow(
                  "main-table", 
                  response.items[i].volumeInfo.title,
                  response.items[i].volumeInfo.authors
               );
            }
   	  });
      
      }
      return false;
   });

   $("#like-button").submit(
      $.ajax({
         url : "/update/",
         type : "POST",
         data : {
            "csrfmiddlewaretoken" : "{{ csrf_token }}",
            pk : JSON.stringify(likes)
         },
         success : function(result) {}
      })
   )

});

var likes = {};

function addRow(tableID, col1, col2) {
   // Get a reference to the table
   let tableRef = document.getElementById(tableID);
 
   // Insert a row at the end of the table
   let newRow = tableRef.insertRow(-1);
 
   // Insert a cell in the row at index 0
   let newCell = newRow.insertCell(0);
 
   // Append a text node to the cell
   let newText = document.createTextNode(col1);
   newCell.appendChild(newText);

   // Insert a cell in the row at index 1
   newCell = newRow.insertCell(1);
 
   // Append a text node to the cell
   newText = document.createTextNode(col2);
   newCell.appendChild(newText);

   // Insert a cell in the row at index 2
   newCell = newRow.insertCell(2);
 
   // Append a text node to the cell
   newText = document.createElement("BUTTON");
   newText.addEventListener("click", function(){
      like(col1);
   });
   newText.innerHTML = "Like";
   newText.setAttribute("id", "like-button")
   likes[col1] = 0;
   newCell.appendChild(newText);
};

function like(title) {
   likes[title]++;
};

function mySort(dict) {
   // Create items array
   var items = Object.keys(dict).map(function(key) {
      return [key, dict[key]];
   });
   
   // Sort the array based on the second element
   items.sort(function(first, second) {
      return second[1] - first[1];
   });
   
   // Create a new array with only the first 5 items
   return items.slice(0, 5);
};

function display() {
   var sortedArray = mySort(likes);
   var top_5 = "Top 5 Books by Likes\n";

   for (let i = 0 ; i<5 ; i++) {
      top_5 += ((i+1) + ". " + sortedArray[i][0] + " with " + sortedArray[i][1] + " likes \n");
   };

   alert(top_5);
};
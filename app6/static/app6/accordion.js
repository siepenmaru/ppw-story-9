$(document).ready(function(){
    update();
    
    $(".accordion_header").click(function(){
       $(".accordion_header").removeClass("active");
       $(this).addClass("active");
    });    

});

var topics = [
    "About Me<button onclick=\"move_up(0)\" >^</button><button name=\"test_down\" onclick=\"move_down(0)\">v</button>",
    "Current Activities<button onclick=\"move_up(1)\" >^</button><button onclick=\"move_down(1)\">v</button>",
    "Organizations<button onclick=\"move_up(2)\" >^</button><button onclick=\"move_down(2)\">v</button>",
    "Achievements<button onclick=\"move_up(3)\">^</button><button onclick=\"move_down(3)\">v</button>"
];

var data = [
    "Born on the 6th of August 2001  &#8226  Likes and dislikes a lot of things",
    "Online Distance Learning  &#8226  Video Gaming  &#8226  Snacking",
    "EDS UI  &#8226  UI MUN  &#8226  PERAK FASILKOM Games Division  &#8226  Compfest Indie Game Ignite Administrative Division",
    "There's a million things I haven\'t done, but just you wait"
];

function update() {
    document.getElementById("header_1").innerHTML = topics[0];
    document.getElementById("body_1").innerHTML = data[0];
    document.getElementById("header_2").innerHTML = topics[1];
    document.getElementById("body_2").innerHTML = data[1];
    document.getElementById("header_3").innerHTML = topics[2];
    document.getElementById("body_3").innerHTML = data[2];
    document.getElementById("header_4").innerHTML = topics[3];
    document.getElementById("body_4").innerHTML = data[3];
};

function move_up(arg) {
    if (arg == 0) {
        return
    }
    else {
        temp_topics = topics[arg].replace(arg, arg-1).replace(arg, arg-1);
        temp_data = data[arg];
        topics[arg] = topics[arg-1].replace(arg-1, arg).replace(arg-1, arg);
        data[arg] = data[arg-1];
        topics[arg-1] = temp_topics;
        data[arg-1] = temp_data;

        update()
        return
    };
};

function move_down(arg) {
    if (arg == 3) {
        return
    }
    else {
        temp_topics = topics[arg].replace(arg, arg+1).replace(arg, arg+1);
        temp_data = data[arg];
        topics[arg] = topics[arg+1].replace(arg+1, arg).replace(arg+1, arg);
        data[arg] = data[arg+1];
        topics[arg+1] = temp_topics;
        data[arg+1] = temp_data;

        update()
        return
    };
};
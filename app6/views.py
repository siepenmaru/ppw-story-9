from django.shortcuts import render, redirect
from .models import Book
from .forms import BookForm

# Create your views here.
def index(request):
    return render(request, "index.html")

def update(request, pk):
    from django.http import JsonResponse
    if request.method=='POST' and request.is_ajax():
        try:
            obj = Book.objects.get(pk=pk)
            obj.data_attr = request.POST['attr_name']
            obj.save()
            return JsonResponse({'status':'Success', 'msg': 'save successfully'})
        except Book.DoesNotExist:
            return JsonResponse({'status':'Fail', 'msg': 'Object does not exist'})
    else:
        return JsonResponse({'status':'Fail', 'msg':'Not a valid request'})
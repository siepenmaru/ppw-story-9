""" from selenium import webdriver
import unittest
import time

class InputTest(unittest.TestCase): #
    def setUp(self): #
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()

    def test_cancel_input(self):
        selenium = self.browser
        # Opening the link we want to test
        selenium.get('http://localhost:8000/')
        time.sleep(5)
        # find the form element
        header_1 = selenium.find_element_by_id('header_1')
        body_1 = selenium.find_element_by_id('body_1')

        move_down = selenium.find_element_by_name('test_down')
        
        move_down.click()
        time.sleep(5)
        
        new_header_1 = selenium.find_element_by_id('header_1')
        new_body_1 = selenium.find_element_by_id('body_1')
        self.assertNotEqual(header_1, new_header_1)
        self.assertNotEqual(body_1, new_body_1)
        
        
if __name__ == '__main__': #
    unittest.main(warnings='ignore') #



 """